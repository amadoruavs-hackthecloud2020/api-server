from django.db import models
from django.contrib.auth.models import AbstractUser

class HealthInfo(models.Model):
    med_notes = models.TextField(blank=True)
    allergies = models.CharField(max_length=420, blank=True)
    blood_type = models.CharField(max_length=3)
    height_inches = models.IntegerField()
    weight_pounds = models.IntegerField()
    primary_language = models.CharField(max_length=420, blank=True)
    name = models.CharField(max_length=128)
    birthdate = models.CharField(max_length=12)
    medications = models.TextField(blank=True)

class Emergency(models.Model):
    modified = models.BooleanField(default=False)
    emergency_type = models.CharField(max_length=30)
    description = models.TextField(blank=True)

    lat = models.DecimalField(max_digits=12, decimal_places=7)
    lon = models.DecimalField(max_digits=12, decimal_places=7)

    health_entry = models.ForeignKey(HealthInfo, on_delete=models.SET_NULL, null=True, blank=True)

class User(AbstractUser):
    USER_TYPE_CHOICES =  (
        ("responder", "responder"),
        ("dispatcher", "dispatcher"),
    )
    user_type = models.CharField(max_length=12, choices=USER_TYPE_CHOICES)
    name = models.CharField(max_length=69)
    lat = models.DecimalField(max_digits=12, decimal_places=7, blank=True, null=True)
    lon = models.DecimalField(max_digits=12, decimal_places=7, blank=True, null=True)
    emergency = models.ForeignKey(Emergency, on_delete=models.SET_NULL, null=True, blank=True)
