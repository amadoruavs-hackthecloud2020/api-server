import json
from channels.generic.websocket import AsyncWebsocketConsumer

class ResponderStatusConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.user = self.scope["user"]

        if self.user is not None:
            await self.channel_layer.group_add(
                "locations",
                self.user.name
            )
        else:
            await self.channel_layer.group_add(
                "locations",
                "listener"
            )

        await self.accept()

    async def disconnect(self, close_code):
        if self.user is not None:
            await self.channel_layer.group_discard(
                "locations",
                self.user.name
            )
        else:
            await self.channel_layer.group_discard(
                "locations",
                "listener"
            )

    async def receive(self, data):
        data_json = json.loads(data)
        event_type = data_json['event_type']

        if self.user.name is not None:
            if event_type == "location":
                await self.channel_layer.group_send(
                    self.user.name,
                    {
                        'type': 'location',
                        'lat': data_json['lat'],
                        'lon': data_json['lon']
                    }
                )
