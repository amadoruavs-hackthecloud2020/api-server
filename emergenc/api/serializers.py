from rest_framework import serializers
from api.models import User, Emergency, HealthInfo

class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['name',
                  'url',
                  'user_type',
                  'username',
                  'lat',
                  'lon',
                  'emergency']

class EmergencySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Emergency
        fields = '__all__'

class HealthInfoSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = HealthInfo
        fields = '__all__'