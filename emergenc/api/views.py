from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.decorators import action
from api.serializers import UserSerializer, EmergencySerializer, HealthInfoSerializer
from api.models import User, Emergency, HealthInfo


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    permission_classes = (IsAuthenticated,)
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer

    @action(detail=False, methods=['POST'])
    def user_from_token(self, request):
        return Response(UserSerializer(request.user, context={'request': request}).data)

class EmergencyViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    queryset = Emergency.objects.all()
    serializer_class = EmergencySerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = '__all__'

    @action(detail=True, methods=['GET'])
    def responders(self, request, pk=0):
        return Response(UserSerializer(User.objects.all().filter(emergency__id=pk), many=True, context={'request': request}).data)

    @action(detail=True, methods=['GET'])
    def healthinfo(self, request, pk=0):
        return Response(HealthInfoSerializer(HealthInfo.objects.all().filter(emergency__id=pk), many=True, context={'request': request}).data)


class HealthInfoViewSet(viewsets.ModelViewSet):
    #permission_classes = (IsAuthenticated,)
    serializer_class = HealthInfoSerializer
    queryset = HealthInfo.objects.all()

    @action(detail=True, methods=['GET'])
    def emergency_contacts(self, request, pk=0):
        return Response(EmergencyContactSerializer(EmergencyContact.objects.all().filter(healthinfo__id=pk), many=True, context={'request': request}).data)
