from django.contrib import admin
from api.models import User, Emergency, HealthInfo

# Register your models here.
admin.site.register(User)
admin.site.register(Emergency)
admin.site.register(HealthInfo)
